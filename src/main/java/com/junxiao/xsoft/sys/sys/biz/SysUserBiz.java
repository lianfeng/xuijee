package com.junxiao.xsoft.sys.sys.biz;

import com.junxiao.xsoft.base.biz.BaseBiz;
import com.junxiao.xsoft.sys.sys.entity.SysUser;

/**
 * 系统用户表biz
 * 
 * @copyright © 2016 大连骏骁网络科技有限公司
 * @author 程旭(cxmail@qq.com)
 * @createDate 2016-01-29
 * @version: V1.0.0
 */
public interface SysUserBiz extends BaseBiz<SysUser> {

}
